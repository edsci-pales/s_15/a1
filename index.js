console.log('Hewow')
// Details
const details = {
    fName: "giga",
    lName: "chad",
    age: 0,
    hobbies : [
        "cycling", "working out", "drawing"
    ] ,
    workAddress: {
        housenumber: "Lot 1, Block 14",
        street: "Corner Street",
        city: "Tree City",
        state: "Forest State",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");